import * as puppeteer from 'puppeteer';

(async () => {


    if(process.argv.length!=3){
        console.log("Usage: ts-node crawl.ts ISBN");
        process.exit(0);
    }

    var isbn = process.argv[2];
    console.log("Query " + isbn);

    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    var query = ('https://www.amazon.com/Advanced-Search-Books/b?ie=UTF8&node=241582011');
    await page.goto(query);

    const html = await page.content();
   
    await page.type('#field-isbn', isbn);
    const submit = await page.$('[name="Adv-Srch-Books-Submit"]');

    await submit.click();
    await page.waitForNavigation();

    var response = await page.content();

    const priceWhole_e = await page.$('span.a-price-whole');
    const priceFraction_e = await page.$('span.a-price-fraction');
    
    if(priceWhole_e !=null && priceFraction_e!=null){

    const priceWhole = await (await priceWhole_e.getProperty('textContent')).jsonValue();
    const priceFraction = await (await priceFraction_e.getProperty('textContent')).jsonValue();

    console.log(<string>priceWhole + <string>priceFraction);
    }

    else{
        console.log("No primary listing");
    }
    
    await browser.close();

})();


